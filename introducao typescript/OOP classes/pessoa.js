"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pessoa = void 0;
var Pessoa = /** @class */ (function () {
    //constructor
    function Pessoa(nome, cpf) {
        this._nome = nome;
        this._cpf = cpf;
    }
    Object.defineProperty(Pessoa.prototype, "nome", {
        //metodos de acesso (getter e setter)
        get: function () {
            if (this._nome = "") {
                return "nome nao atruido";
            }
            this._nome;
        },
        set: function (nome) {
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "cpf", {
        get: function () {
            return this._cpf;
        },
        set: function (cpf) {
            if (this.validarCpf(cpf)) {
                this._cpf = cpf;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "idade", {
        get: function () {
            return this._idade;
        },
        set: function (idade) {
            this._idade = idade;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "endereco", {
        get: function () {
            return this._endereco;
        },
        set: function (endereco) {
            this._endereco = endereco;
        },
        enumerable: false,
        configurable: true
    });
    //metodos da classe - cmportamento da classe
    Pessoa.prototype.cumprimentar = function (nome) {
        console.log("Ol\u00E1, " + nome + " meu nome \u00E9 " + this._nome);
        console.log("Meu cpf " + this.cpf);
    };
    Pessoa.prototype.validarCpf = function (cpf) {
        if (cpf == "") {
            return false;
        }
        else {
            return true;
        }
    };
    return Pessoa;
}());
exports.Pessoa = Pessoa;
