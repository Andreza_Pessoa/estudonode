export class Pessoa {

    //atrubutos
    //modificadores de acesso --- public, private, protected
    private _nome: string;
    private _cpf: string;
    private _idade: number;
    private _endereco: string;

    //constructor
    constructor(nome: string, cpf: string) {
        this._nome = nome;
        this._cpf = cpf;
    }

    //metodos de acesso (getter e setter)
    public  get nome() {
        if (this._nome = "") {
            return "nome nao atruido"
        }
        this._nome
    }
    public set nome(nome: string) {

        this._nome = nome
    }

    public get cpf() {
        return this._cpf
    }
    public set cpf(cpf: string) {
        if (this.validarCpf(cpf)) {
            this._cpf = cpf
        }
    }

    public get idade() {
        return this._idade
    }
    public set idade(idade: number) {
        this._idade = idade
    }

    public get endereco() {
        return this._endereco
    }
    public set endereco(endereco: string) {
        this._endereco = endereco
    }

    //metodos da classe - cmportamento da classe
    public cumprimentar(nome: string) {
        console.log(`Olá, ${nome} meu nome é ${this._nome}`);
        console.log(`Meu cpf ${this.cpf}`);

    }
    private validarCpf(cpf: string) {
        if (cpf == "") {
            return false
        } else {
            return true
        }
    }
}