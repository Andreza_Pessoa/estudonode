"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.cadastroJuridica = void 0;
var pessoa_1 = require("./pessoa");
var cadastroJuridica;
(function (cadastroJuridica) {
    var PessoaJuridica = /** @class */ (function (_super) {
        __extends(PessoaJuridica, _super);
        function PessoaJuridica() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(PessoaJuridica.prototype, "cnpj", {
            get: function () {
                return this._cnpj;
            },
            set: function (value) {
                this._cnpj = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(PessoaJuridica.prototype, "inscricaoEstadual", {
            get: function () {
                return this._inscricaoEstadual;
            },
            set: function (value) {
                this._inscricaoEstadual = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(PessoaJuridica.prototype, "dataFundacao", {
            get: function () {
                return this._dataFundacao;
            },
            set: function (value) {
                this._dataFundacao = value;
            },
            enumerable: false,
            configurable: true
        });
        return PessoaJuridica;
    }(pessoa_1.pessoaUser.Pessoa));
    cadastroJuridica.PessoaJuridica = PessoaJuridica;
})(cadastroJuridica || (cadastroJuridica = {}));
exports.cadastroJuridica = cadastroJuridica;
