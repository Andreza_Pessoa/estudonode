import { enderecoUser } from "./endereco";


namespace pessoaUser{

    export class Pessoa {
    
        private _nome: string;
        private _endereco: enderecoUser.Endereco;
        private _telefone: string;
        
    
        //metodo construtor
        constructor(nome: string, endereco: enderecoUser.Endereco, telefone: string){
            this.nome = nome
            this.endereco = endereco
            this.telefone = telefone
        }
    
        //metodo assessor
        public get nome(): string {
            return this._nome;
        }
        //metodo mutattor
        public set nome(value: string) {
            this._nome = value;
        }
    
        public get endereco(): enderecoUser.Endereco {
            return this._endereco;
        }
        public set endereco(value: enderecoUser.Endereco) {
            this._endereco = value;
        }
    
        protected get telefone(): string {
            return this._telefone;
        }
        protected set telefone(value: string) {
            this._telefone = value;
        }
    
        //metodo worker
        public calculaIdade(): number{
            return 21
        }
    }
}

export {pessoaUser}