import { pessoaUser } from "./pessoa";

namespace cadastroJuridica{
    export class PessoaJuridica extends pessoaUser.Pessoa{
    
        private _cnpj: string;
        private _inscricaoEstadual: string; 
        private _dataFundacao: string;
        
    
        public get cnpj(): string {
            return this._cnpj;
        }
        public set cnpj(value: string) {
            this._cnpj = value;
        }
    
        public get inscricaoEstadual(): string {
            return this._inscricaoEstadual;
        }
        public set inscricaoEstadual(value: string) {
            this._inscricaoEstadual = value;
        }
    
        public get dataFundacao(): string {
            return this._dataFundacao;
        }
        public set dataFundacao(value: string) {
            this._dataFundacao = value;
        }
        
    }

}

export { cadastroJuridica}