"use strict";
// import { Endereco } from "./endereco";
// import { PessoaFisica } from "./pessoaFisica";
// import { PessoaJuridica } from "./pessoaJuridica";
Object.defineProperty(exports, "__esModule", { value: true });
//      let endereco = new Endereco("sao paulo", "sao paulo", "rubens lara", "120", 66, "11533520")
//     let pessoaFisica = new PessoaFisica("000.000.000-0", "0000000", "2021-09-22", "fulano", endereco, "(55) 1399-8975")
//     let pessoaJuridica = new PessoaJuridica("000.000.000-0", endereco,"2021-09-22")
//     //pessoaFisica.telefone = "(55) 1399-8975"
//     console.log(pessoaFisica.nome);
//     console.log(pessoaFisica.calculaIdade());
//     console.log(endereco);
//     console.log(pessoaFisica);
//     console.log(pessoaJuridica);
var endereco_1 = require("./endereco");
var pessoaFisica_1 = require("./pessoaFisica");
var pessoaJuridica_1 = require("./pessoaJuridica");
var endereco = new endereco_1.enderecoUser.Endereco("sao paulo", "sao paulo", "rubens lara", "120", 66, "11533520");
var pessoaFisica = new pessoaFisica_1.cadastroFisica.PessoaFisica("000.000.000-0", "0000000", "2021-09-22", "fulano", endereco, "(55) 1399-8975");
var pessoaJuridica = new pessoaJuridica_1.cadastroJuridica.PessoaJuridica("000.000.000-0", endereco, "2021-09-22");
//pessoaFisica.telefone = "(55) 1399-8975"
console.log("nome da pessoa fisica " + pessoaFisica.nome);
console.log("idade da pessoa fisica " + pessoaFisica.calculaIdade());
console.log(endereco);
console.log(pessoaFisica);
console.log(pessoaJuridica);
