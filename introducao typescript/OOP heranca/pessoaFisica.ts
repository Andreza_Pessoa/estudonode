import { enderecoUser } from "./endereco";
import { pessoaUser } from "./pessoa";

namespace cadastroFisica{
    export class PessoaFisica extends pessoaUser.Pessoa {
    
        private _cpf: string;
        private _rg: string;
        private _dataNasc: string;
         static calculaIdade: any;
         static nome: any;
    
        //metodo construtor que herda da classe pessoa
        constructor(cpf: string, rg: string, dataNasc: string, nomePessoa: string, enderecoPessoa: enderecoUser.Endereco, telefone: string) {
            super(nomePessoa, enderecoPessoa, telefone);
            this.cpf = cpf;
            this.rg = rg;
            this.dataNasc = dataNasc;
        }
    
        public get cpf(): string {
            return this._cpf;
        }
        public set cpf(value: string) {
            this._cpf = value;
        }
    
        public get rg(): string {
            return this._rg;
        }
        public set rg(value: string) {
            this._rg = value;
        }
    
        public get dataNasc(): string {
            return this._dataNasc;
        }
        public set dataNasc(value: string) {
            this._dataNasc = value;
        }
    
        // public telefonar(telefone: string){
        //     this.telefone = telefone
        // }
    
    }

}

export { cadastroFisica}