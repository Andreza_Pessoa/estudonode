"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.enderecoUser = void 0;
var enderecoUser;
(function (enderecoUser) {
    var Endereco = /** @class */ (function () {
        function Endereco(estado, cidade, bairro, rua, numero, cep) {
            this.estado = estado;
            this.cidade = cidade;
            this.bairro = bairro;
            this.rua = rua;
            this.numero = numero;
            this.cep = cep;
        }
        Object.defineProperty(Endereco.prototype, "estado", {
            get: function () {
                return this._estado;
            },
            set: function (value) {
                this._estado = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Endereco.prototype, "cidade", {
            get: function () {
                return this._cidade;
            },
            set: function (value) {
                this._cidade = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Endereco.prototype, "bairro", {
            get: function () {
                return this._bairro;
            },
            set: function (value) {
                this._bairro = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Endereco.prototype, "rua", {
            get: function () {
                return this._rua;
            },
            set: function (value) {
                this._rua = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Endereco.prototype, "numero", {
            get: function () {
                return this._numero;
            },
            set: function (value) {
                this._numero = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Endereco.prototype, "cep", {
            get: function () {
                return this._cep;
            },
            set: function (value) {
                this._cep = value;
            },
            enumerable: false,
            configurable: true
        });
        return Endereco;
    }());
    enderecoUser.Endereco = Endereco;
})(enderecoUser || (enderecoUser = {}));
exports.enderecoUser = enderecoUser;
