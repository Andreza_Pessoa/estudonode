"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.cadastroFisica = void 0;
var pessoa_1 = require("./pessoa");
var cadastroFisica;
(function (cadastroFisica) {
    var PessoaFisica = /** @class */ (function (_super) {
        __extends(PessoaFisica, _super);
        //metodo construtor que herda da classe pessoa
        function PessoaFisica(cpf, rg, dataNasc, nomePessoa, enderecoPessoa, telefone) {
            var _this = _super.call(this, nomePessoa, enderecoPessoa, telefone) || this;
            _this.cpf = cpf;
            _this.rg = rg;
            _this.dataNasc = dataNasc;
            return _this;
        }
        Object.defineProperty(PessoaFisica.prototype, "cpf", {
            get: function () {
                return this._cpf;
            },
            set: function (value) {
                this._cpf = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(PessoaFisica.prototype, "rg", {
            get: function () {
                return this._rg;
            },
            set: function (value) {
                this._rg = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(PessoaFisica.prototype, "dataNasc", {
            get: function () {
                return this._dataNasc;
            },
            set: function (value) {
                this._dataNasc = value;
            },
            enumerable: false,
            configurable: true
        });
        return PessoaFisica;
    }(pessoa_1.pessoaUser.Pessoa));
    cadastroFisica.PessoaFisica = PessoaFisica;
})(cadastroFisica || (cadastroFisica = {}));
exports.cadastroFisica = cadastroFisica;
