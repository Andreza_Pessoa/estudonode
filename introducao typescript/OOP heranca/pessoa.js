"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pessoaUser = void 0;
var pessoaUser;
(function (pessoaUser) {
    var Pessoa = /** @class */ (function () {
        //metodo construtor
        function Pessoa(nome, endereco, telefone) {
            this.nome = nome;
            this.endereco = endereco;
            this.telefone = telefone;
        }
        Object.defineProperty(Pessoa.prototype, "nome", {
            //metodo assessor
            get: function () {
                return this._nome;
            },
            //metodo mutattor
            set: function (value) {
                this._nome = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Pessoa.prototype, "endereco", {
            get: function () {
                return this._endereco;
            },
            set: function (value) {
                this._endereco = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Pessoa.prototype, "telefone", {
            get: function () {
                return this._telefone;
            },
            set: function (value) {
                this._telefone = value;
            },
            enumerable: false,
            configurable: true
        });
        //metodo worker
        Pessoa.prototype.calculaIdade = function () {
            return 21;
        };
        return Pessoa;
    }());
    pessoaUser.Pessoa = Pessoa;
})(pessoaUser || (pessoaUser = {}));
exports.pessoaUser = pessoaUser;
