

const express = require("express")

const app = express()

const porta = 3001

app.listen(porta, function(){
    console.log(`servidor rodando na porta ${porta}`);
})

app.get("/", function (req, resp) {
    resp.send(
    `
        <html>
            <head>
                <meta charseat="utf-8"/>
            </head>
            <body>
                <h1>Primeiro servidor em Node Express</h1>
            </body>
        </html>
   `
    )

})

//enviando um string 
app.get("/livros", function (req, resp) {
    var livro = "clean code"
    resp.send(
    `
        <html>
            <head>
                <meta charseat="utf-8"/>
            </head>
            <body>
                <h1> servidor em Node Express->  livros favoritos</h1>
                <p>${livro}</p>
                <button type="button"  class="btn btn-outline-dark" id="idEnviar">Enviar</button>
                <script>
                var btEnviar = document.getElementById("idEnviar")
                </script>
            </body>
        </html>
   `
    )

})
//enviando um javascript object como json
app.get("/usuarios", function (req, resp) {
    const users = ["Andreza", "Carlos", "Alberto" ]
    resp.json({users})
})

//rederizando paginas HTML a partir de arquivos...
app.get("/sobre", function (req, resp) {
   
    resp.sendFile(__dirname + "/html/sobre.html")

})

//rota dinamica
app.get("/endereco/:cep:cidade", function(req, resp){
    // -> chamada para via cep
    resp.send(req.params)
   // resp.send("<h1> CEP: " + req.params.cep + " </h1>")
})

// // buscar cep pela api
// app.get("/endereco/:cep", function(req, resp){

//  var endereco = await buscarEnd(req.params.cep)
//   resp.send(endereco.data)
//   console.log(endereco.data);
//   resp.send(end)

 
// })

async function buscarEnd(cep){
    
    var axios = require("axios").default;
    var options = {
        method: 'GET',
        url:  "https://viacep.com.br/ws/${cep}/json/"
    }
    return await axios.request(options)
}