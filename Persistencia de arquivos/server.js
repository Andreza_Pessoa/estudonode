
const express = require("express")
const fs = require("fs")

const app = express()

//intercepita a requisição
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

const porta = 3001
app.listen(porta, function () {
    console.log(`servidor rodando na porta ${porta}`);
})

app.get("/", function (req, resp) {
    resp.sendFile(__dirname + "/html/cadLivros.html")
})

app.post("/livros", function (req, resp) {

    console.log(req.body);
    // receber dados do formulario
    
    let cadastro = req.body.nmLivro + "," + req.body.nmAutor+ ",\n"
    // salvar os dados do formulario em um arquivo

    fs.appendFile("livros.txt", cadastro ,"\n", function(err){
        if(err){
            throw err
        }else{
            console.log("salvou o livro " + cadastro);
        }
    })
    // resp.sendFile(__dirname + "/html/cadLivros.html")
    resp.send("salvo com sucesso...")

})

app.get("/livros/:cod", function (req, resp) {


    fs.readFile('livros.txt', 'utf8', function(err,data){
        //enviando para o console a resposta
        console.log(data);
        var livro = data.split(",\n")
        console.log(livro);

        let livros = []
       livro.forEach(element => {
           livros.push(element.split(",")) 
        });

        console.log(livros);
        //resp.send("livros: " + data)
        resp.send(livros[req.params.cod])
    })
   

})


app.post("/livros/:autor/:book", function (req, resp) {

   
    
    let cadastro = req.body.nmLivro + "," + req.body.nmAutor+ ",\n"
    // salvar os dados do formulario em um arquivo

    fs.appendFile("livros.txt", cadastro ,"\n", function(err){
        if(err){
            throw err
        }else{
            console.log("salvou o livro " + cadastro);
        }
    })
    // resp.sendFile(__dirname + "/html/cadLivros.html")
    resp.send("salvo com sucesso...")

})
